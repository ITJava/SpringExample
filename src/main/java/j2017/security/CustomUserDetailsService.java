package j2017.security;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import j2017.model.Profile;
import j2017.repository.ProfileRepository;
import j2017.repository.UserRolesRepository;

@Service("customUserdetailsService")
public class CustomUserDetailsService implements UserDetailsService {
	private ProfileRepository profileRepo;
	private UserRolesRepository userRoleRepo;

	@Autowired
	public CustomUserDetailsService(ProfileRepository profileRepo, UserRolesRepository userRoleRepo) {
		this.profileRepo = profileRepo;
		this.userRoleRepo = userRoleRepo;
	}

	@Override
	public UserDetails loadUserByUsername(String nickname) throws UsernameNotFoundException {
		Optional<Profile> profile = Optional.ofNullable(profileRepo.findByProfileNickname(nickname));

		if (profile.isPresent()) {
			List<String> userRoles = userRoleRepo.findRoleByNickname(nickname);
			return new CustomUserDetails(profile.get(), userRoles);
		} else {
			throw new UsernameNotFoundException("No user present with username: " + nickname);
		}
	}

}
