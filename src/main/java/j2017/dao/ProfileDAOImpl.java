package j2017.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import j2017.model.Profile;

@Transactional
@Repository
public class ProfileDAOImpl implements IProfileDAO {
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<Profile> allProfiles() {
		String hql = "FROM Profiles as prof ORDER BY prof.profile_id";
		return (List<Profile>) entityManager.createQuery(hql).getResultList();
	}

	@Override
	public Profile getProfileById(long id) {
		return entityManager.find(Profile.class, id);
	}

	@Override
	public void addProfile(Profile profile) {
		entityManager.persist(profile);
	}

	@Override
	public void updateProfile(Profile profile) {
		entityManager.refresh(profile);
	}

	@Override
	public void deleteProfile(long id) {
		entityManager.remove(id); 
	}

	@Override
	public boolean profileExists(String email) {
		String hql = "FROM Profiles as prof1 WHERE prof1.email = ?";
		int count = entityManager.createQuery(hql).setParameter(1, email)
		              .getResultList().size();
		return count > 0 ? true : false;
	}

}
