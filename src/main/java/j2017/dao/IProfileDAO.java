package j2017.dao;

import java.util.List;

import j2017.model.Profile;

public interface IProfileDAO {
	List<Profile> allProfiles();
	Profile getProfileById(long id);
	void addProfile(Profile profile);
	void updateProfile(Profile profile);
	void deleteProfile(long id);
	boolean profileExists( String email);
}
