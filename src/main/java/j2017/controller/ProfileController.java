package j2017.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

import j2017.model.Profile;
import j2017.service.IProfileService;

@Controller
@RequestMapping
public class ProfileController {
	@Autowired
	private IProfileService profileService;
	
	@GetMapping("profile/{id}")
	public ResponseEntity<Profile> getProfilesById(@PathVariable("id") Integer id) {
		Profile profile = profileService.getProfileById(id);
		return new ResponseEntity<Profile>(profile, HttpStatus.OK);
	}

	@GetMapping("profile")
	public ResponseEntity<List<Profile>> getAllProfiles() {
		List<Profile> list = profileService.allProfiles();
		return new ResponseEntity<List<Profile>>(list, HttpStatus.OK);
	}

	@PostMapping("profile")
	public ResponseEntity<Void> addProfile(@RequestBody Profile profile, UriComponentsBuilder builder) {
		boolean flag = profileService.addProfile(profile);
		if (flag == false) {
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(builder.path("/profile/{id}").buildAndExpand(profile.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	@PutMapping("profile")
	public ResponseEntity<Profile> updateProfile(@RequestBody Profile profile) {
		profileService.updateProfile(profile);
		return new ResponseEntity<Profile>(profile, HttpStatus.OK);
	}

	@DeleteMapping("profile/{id}")
	public ResponseEntity<Void> deleteProfile(@PathVariable("id") Integer id) {
		profileService.deleteProfile(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
}
