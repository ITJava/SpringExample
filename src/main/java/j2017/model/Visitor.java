package j2017.model;

public class Visitor {
	private long id;
	private long visitsCount;

	
	public Visitor() {
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public long getVisitsCount() {
		return visitsCount;
	}

	public void setVisitsCount(long visitsCount) {
		this.visitsCount = visitsCount;
	}

	@Override
	public String toString() {
		return "Visitor [id=" + id + ", visitsCount=" + visitsCount + "]";
	}

	
}
