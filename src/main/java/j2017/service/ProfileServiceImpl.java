package j2017.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import j2017.dao.IProfileDAO;
import j2017.model.Profile;

@Service
public class ProfileServiceImpl implements IProfileService {
	@Autowired
	private IProfileDAO profileDAO;

	@Override
	public List<Profile> allProfiles() {
		return profileDAO.allProfiles();
	}

	@Override
	public Profile getProfileById(long id) {
		return profileDAO.getProfileById(id);
	}

	@Override
	public boolean addProfile(Profile profile) {
		profileDAO.addProfile(profile);
		return true;
	}

	@Override
	public void updateProfile(Profile profile) {
		profileDAO.updateProfile(profile);
	}

	@Override
	public void deleteProfile(long id) {
		profileDAO.deleteProfile(id);
	}

}
