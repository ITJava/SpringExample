package j2017.service;

import java.util.List;

import j2017.model.Profile;

public interface IProfileService {
	List<Profile> allProfiles();
	Profile getProfileById(long id);
	boolean addProfile(Profile profile);
	void updateProfile(Profile profile);
	void deleteProfile(long id);
}
