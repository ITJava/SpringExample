package j2017;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import j2017.model.Comment;
import j2017.model.GenderEnum;
import j2017.model.Profile;
import j2017.repository.CommentRepository;
import j2017.repository.ProfileRepository;

@Controller
public class HelloController {
	
	@Autowired
	private ProfileRepository repository;
	@Autowired
	private CommentRepository commentRepo;
	
	
	@RequestMapping("/foo")
    public String index(HttpServletRequest request, Model model) {
    	long id = request.getSession().getId().hashCode();
    	Profile profile = new Profile();
    	Comment comment = new Comment();
    	comment.setTitle("Big Pig");
    	comment.setContent("Today I saw big pig. Nice day");
    	Comment comment2 = new Comment();
    	comment2.setTitle("Little duck");
    	comment2.setContent("Today I ate big pig and little duck. One more nice day");
    	Comment comment3 = new Comment();
    	comment3.setTitle("Worst day");
    	comment3.setContent("Today I have adventure in outhouse");
    	commentRepo.save(comment);
    	commentRepo.save(comment2);
    	commentRepo.save(comment3);
    	comment = commentRepo.getOne(1l);
    	comment2 = commentRepo.getOne(2l);
    	comment3 = commentRepo.getOne(3l);
    	profile.setFirstName("Robert Polson");
    	profile.setGender(GenderEnum.MAN);
    	profile.getComments().add(comment);
    	profile.getComments().add(comment2);
    	profile.getComments().add(comment3);
    	repository.save(profile);
    	Profile p = repository.findAll().stream().findFirst().get();
    	model.addAttribute("profile", p);
        return "HelloThymeleaf";
    }
	
	@RequestMapping("/createUser")
	@ResponseBody
    public String createUser(HttpServletRequest request, Model model) {
    	Profile profile = new Profile();
    	profile.setFirstName("Robert Polson");
    	profile.setGender(GenderEnum.MAN);
    	profile = repository.save(profile);
    	Comment comment = new Comment();
    	comment.setTitle("Little duck");
    	comment.setContent("Today I ate big pig and little duck. One more nice day");
    	comment = commentRepo.save(comment);
    	profile.getComments().add(comment);
    	comment = new Comment();
    	comment.setTitle("Big Pig");
    	comment.setContent("Today I saw big pig. Nice day");
    	comment = commentRepo.save(comment);
    	profile.getComments().add(comment);
    	comment = new Comment();
    	comment.setTitle("Worst day");
    	comment.setContent("Today I have adventure in outhouse");
    	comment = commentRepo.save(comment);
    	profile.getComments().add(comment);
    	
    	profile = new Profile();
    	profile.setFirstName("Barbara Z");
    	profile.setGender(GenderEnum.WOMAN);
    	profile = repository.save(profile);
    	comment = new Comment();
    	comment.setTitle("Barbara say BLA");
    	comment.setContent("BLA-BLA-BLA-BLA-BLA");
    	comment = commentRepo.save(comment);
    	profile.getComments().add(comment);
    	comment = new Comment();
    	comment.setTitle("Comment lol");
    	comment.setContent("Babrbara say 'LOL'. AZAZAZAZA");
    	comment = commentRepo.save(comment);
    	profile.getComments().add(comment);
    	repository.flush();
        return "Ok";
    }
	
	@RequestMapping("/getCommentByProfile/{id}")
    public String getComments(HttpServletRequest request, 
    		Model model, @PathVariable(value = "id", required = true) long id) {    
    	Profile p = repository.getOne(id);
    	List<Comment> comments = commentRepo.getCommentsByProfile(p);
    	model.addAttribute("comments", comments);
        return "HelloThymeleaf";
    }

}
