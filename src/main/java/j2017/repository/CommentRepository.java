package j2017.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import j2017.model.Comment;
import j2017.model.Profile;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long>{
	
	@Query("select c from Comment c where c.profile.id = :#{#profile.id}")
	List<Comment> getCommentsByProfile(@Param("profile") Profile profile);
}
