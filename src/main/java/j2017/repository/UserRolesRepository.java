package j2017.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import j2017.model.UserRole;

@Repository
public interface UserRolesRepository extends JpaRepository<UserRole, Long> {
	@Query("select ur.role from UserRole ur, Profile p where p.nickname=?1 and ur.userId=p.id")
	public List<String> findRoleByNickname(String nickname);
}
