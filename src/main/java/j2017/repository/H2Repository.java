package j2017.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import j2017.model.Visitor;

@Transactional
@Repository
@Scope("prototype")
public class H2Repository {
	@Autowired
	JdbcTemplate jdbcTemplate;
	
//	@PostConstruct
//	public void init() {
//		jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS Visitors " 
//	+ "(id BIGINT UNIQUE NOT NULL, visits_count BIGINT, PRIMARY KEY (id))");
//	}
	
	public List<Visitor> getVisitors(){
		return jdbcTemplate.query("SELECT * FROM Visitors", new RowMapper<Visitor>() {
			@Override
			public Visitor mapRow(ResultSet rs, int rowNum) throws SQLException {
				Visitor visitor = new Visitor();
				visitor.setVisitsCount(rs.getLong("visits_count"));
				visitor.setId(rs.getLong("id"));
				return visitor;
			}
		});
	}
	
	public void setCount(long id, long count) {
		jdbcTemplate.execute("UPDATE Visitors SET visits_count = " + count
				+ " WHERE id = " + id);
	}
	
	public void addVisitor(long id) {
		String sql = "INSERT INTO Visitors (id, visits_count) VALUES ("
				+ id + ", " + 0 + ")";
		jdbcTemplate.execute(sql);
	}
	
	public Visitor getVisitor(long id) {
		String sql = "SELECT * FROM Visitors WHERE id = ?";
		Visitor visitor = (Visitor)jdbcTemplate.queryForObject(
				sql, new Object[] { id }, 
				new BeanPropertyRowMapper(Visitor.class));
		
		return visitor;
	}
}
