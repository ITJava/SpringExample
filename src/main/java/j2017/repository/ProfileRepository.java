package j2017.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import j2017.model.Profile;

@Repository
public interface ProfileRepository extends JpaRepository<Profile, Long> {
	public Profile findByProfileNickname(String nickname);

}
